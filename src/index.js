import React from "react";
import ReactDOM from "react-dom";
import { ThemeProvider } from "styled-components";
import { FontFace } from "./styles/settings/font-face";
import GlobalStyle from "./styles/global-style";
import theme from "./styles/theme";
import Linktree from "./components/linktree/linktree";

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <FontFace />
      <GlobalStyle />
      <Linktree />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
