// I've imported the sketch file to figma but I think I'm viewing it in edit mode
// as opposed to dev/handover mode.
// Its not giving me the css styles so I'm estimating where I'm unable to measure easily.

const theme = {
	// @todo break these out into their own files for easier management

	font: {
		family: {
			primary: '"Karla", sans-serif',
		},
	},
	spacing: {
		small: "8px",
		medium: "16px",
		large: "24px",
		xlarge: "48px",
	},
	// @todo I might remap these to a more reusable naming convention
	// eg. primary / secondary / tertiary.
	// since the users can customize their colors we might want to
	// import user generated theme. or css variables. Usually I would
	// set and get them from a CMS. I'd look whats possible with
	// themes and/or CSS variables. This may also depend on existing tools/frameworks.
	// I've seen Material UI looks like it might have a good way of managing themes.
	color: {
		black: "#263238",
		grey: "#DADEE0",
		greyLight: "#F5F7F8",
		green: "#39E09B",
		white: "#ffffff",
	},
};
export default theme;
