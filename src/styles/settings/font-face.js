import { createGlobalStyle } from "styled-components";
import Karla from "../../assets/Karla-Regular.woff";

export const FontFace = createGlobalStyle`
    @font-face {
        font-family: "Karla";
        src: url(${Karla}) format('woff');
        font-weight: 400;
        font-style: normal;
    }
`;
