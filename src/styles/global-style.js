import { createGlobalStyle } from "styled-components";
import { reset } from "styled-reset";
import { normalize } from "styled-normalize";

const GlobalStyle = createGlobalStyle`
	${reset}
	${normalize}

	html {
		box-sizing: border-box;
    	overflow-x: hidden;
	}
	*, *:before, *:after {
		box-sizing: inherit;
	}

	// @todo responsive styles (fixing these so its nicer to look at)
	body {
		margin: 0;
		font-family: ${({ theme }) => theme.font.family.primary};
		// background-color: ${({ theme }) => theme.color.greyLight}
	}

	.u-mb-small {
		margin-bottom: ${({ theme }) => theme.spacing.small}
	}
	
	.u-mb-medium {
		margin-bottom: ${({ theme }) => theme.spacing.medium}
	}
`;

export default GlobalStyle;
