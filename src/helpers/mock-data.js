import profilePicture from "../assets/profile-picture.png";
import Spotify from "../assets/icons/spotify.svg";
import AppleMusic from "../assets/icons/apple-music.svg";
import Soundcloud from "../assets/icons/soundcloud.svg";
import YouTube from "../assets/icons/youtube.svg";
import Deezer from "../assets/icons/deezer.svg";
import Tidal from "../assets/icons/tidal.svg";
import Bandcamp from "../assets/icons/bandcamp.svg";

const user = {
	username: "@username",
	profileImage: {
		src: profilePicture,
		alt: "alt",
		title: "title",
	},
};

const links = [
	{
		type: "classic",
		url: "https://linktr.ee/guardian",
		title: "Classic Link",
	},
	{
		type: "musicPlayer",
		title: "Music Player",
		links: [
			{
				title: "Spotify",
				url: "https://spotify.com",
				imgSrc: Spotify,
			},
			{
				title: "Apple Music",
				url: "https://apple.com",
				imgSrc: AppleMusic,
			},
			{
				title: "Soundcloud",
				url: "https://soundcloud.com",
				imgSrc: Soundcloud,
			},
			{
				title: "YouTube Music",
				url: "https://youtube.com",
				imgSrc: YouTube,
			},
			{ title: "Deezer", url: "https://deezer.com", imgSrc: Deezer },
			{ title: "Tidal", url: "https://tidal.com", imgSrc: Tidal },
			{
				title: "Bandcamp",
				url: "https://bandcamp.com",
				imgSrc: Bandcamp,
			},
		],
	},
	// @todo fix this json from auto adding trailing comma
	{
		type: "showsList",
		title: "Shows List",
		shows: [
			{
				date: "Apr 01 2019",
				location: "The Forum, Melbourne",
				url: "http://songkick.com",
			},
			{
				date: "Apr 02 2019",
				url: "http://songkick.com",
				location: "Venue Name, Canberra",
				tickets: "Sold out",
			},
			{
				date: "Apr 02 2019",
				location: "Venue Name, Sydney",
				url: "http://songkick.com",
			},
			{
				date: "Apr 04 2019",
				location: "Venue Name, Brisbane",
				url: "http://songkick.com",
			},
		],
	},
];

export { user, links };
