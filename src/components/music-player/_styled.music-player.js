import styled from "styled-components";
// this is probably not the best solution to manage the reuse of styles
// the best solutions depends on the environment and other variables
// for the purpose of this demo I will import and extend classic
// I'm not sold on this approach but I will include it as an example.
import { Classic } from "../classic/_styled.classic";

export const MusicPlayer = styled.div`
	margin-bottom: ${({ theme }) => theme.spacing.medium};
`;

export const Link = styled(Classic)`
	border: none;
	cursor: pointer;
`;

export const Providers = styled.div`
	border-radius: 4px;
	background-color: ${({ theme }) => theme.color.greyLight};
	position: relative;

	// not good idea to target elements but I'm trying to be consise
	// and I'm not considering having to scale or maintain this

	header {
		padding: ${({ theme }) => theme.spacing.medium};
		display: flex;
		align-items: center;
		justify-content: center;
	}

	li {
		display: flex;
		align-items: stretch;

		a {
			text-decoration: none;
			color: ${({ theme }) => theme.color.black};
			display: flex;
			align-items: center;
			justify-content: space-between;
			height: 100%;
			font-size: 20px; //@todo figma says 28px but it doesn't look right.
			border-bottom: 1px solid ${({ theme }) => theme.color.grey};
			// @todo disable border on last anchor;

			&:hover {
				opacity: 0.8;
			}

			> svg {
				transform: rotate(270deg) translateY(-16px);
			}
		}
	}
`;

export const Image = styled.div`
	padding: ${({ theme }) => theme.spacing.medium};
	display: flex;
	align-items: center;
	justify-content: center;

	// @todo this should be an svg with fill but I included the
	// assets how I might expect to get them from an api.
	// I could redo this and render them as an SVG but
	// there is quite a lot to do in a small amount of time.

	img {
		opacity: 66%;
	}
`;
