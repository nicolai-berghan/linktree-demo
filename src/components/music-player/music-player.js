import React from "react";
import DropDown from "../dropdown/dropdown";
import * as s from "./_styled.music-player";
import { ReactComponent as SongKick } from "../../assets/icons/songkick.svg";
import { ReactComponent as Arrow } from "../../assets/icons/arrow.svg";

const MusicPlayer = ({ link, musicPlayerIsOpen, toggleMusicPlayer }) => {
	const { title, links } = link;

	return (
		<s.MusicPlayer>
			<s.Link as="button" onClick={() => toggleMusicPlayer()}>
				{title}
			</s.Link>
			<DropDown isOpen={musicPlayerIsOpen}>
				<s.Providers>
					<header>
						{/* 
							@todo there were no music player assets
							so I asssume this will suffice
						*/}
						<button
							onClick={() =>
								alert("👩‍🎤 beep boop a doo be doo bop bop 💃")
							}
						>
							Music Player Button
						</button>
					</header>
					<ul>
						{links.map((link, i) => (
							<li key={i}>
								<s.Image>
									<img src={link.imgSrc} alt="alt" />
								</s.Image>
								{/* 
									sometimes I think you may as well inline
									styles instead of generate utilities									
								*/}
								<div style={{ flex: 1 }}>
									<a
										href={link.url}
										target="_blank"
										rel="noreferrer"
									>
										<span>{link.title}</span>
										<Arrow />
									</a>
								</div>
							</li>
						))}
					</ul>
				</s.Providers>
			</DropDown>
		</s.MusicPlayer>
	);
};

export default MusicPlayer;
