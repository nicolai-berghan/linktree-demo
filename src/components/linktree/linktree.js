import React from "react";
import * as s from "./_styled.linktree";
import Profile from "../profile/profile";
import Links from "../links/links";
import Footer from "../footer/footer";

// @todo probably rename this to app.js and move out of the components folder
const Linktree = () => {
	return (
		<s.Linktree>
			<Profile />
			<Links />
			<Footer />
		</s.Linktree>
	);
};

export default Linktree;
