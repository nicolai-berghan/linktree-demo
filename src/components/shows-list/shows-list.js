import React from "react";
import DropDown from "../dropdown/dropdown";
import * as s from "./_styled.shows-list";
import { ReactComponent as SongKick } from "../../assets/icons/songkick.svg";
import { ReactComponent as Arrow } from "../../assets/icons/arrow.svg";

const ShowsList = ({ link, showsListIsOpen, toggleShowsList }) => {
	const { title, shows } = link;

	return (
		<s.ShowsList>
			<s.Link as="button" onClick={() => toggleShowsList()}>
				{title}
			</s.Link>
			<DropDown isOpen={showsListIsOpen}>
				<s.Shows>
					<ul>
						{shows.map((link, i) => {
							return (
								<li key={i}>
									{/* @todo consider disabling anchor if no tickets available*/}
									<a
										href={link.url}
										target="_blank"
										rel="noreferrer"
									>
										{/*
										@todo consider whether using classnames is
								 		appropriate when using styled components 
								 	*/}
										<div>
											<span className="date">
												{link.date}
											</span>
											<span className="location">
												{link.location}
											</span>
										</div>

										{link.tickets ? (
											<span className="tickets">
												{link.tickets}
											</span>
										) : (
											<Arrow />
										)}
									</a>
								</li>
							);
						})}
					</ul>
					<footer>
						<SongKick />
					</footer>
				</s.Shows>
			</DropDown>
		</s.ShowsList>
	);
};

export default ShowsList;
