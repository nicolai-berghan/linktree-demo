import styled from "styled-components";
import { Classic } from "../classic/_styled.classic";

export const ShowsList = styled.div`
	margin-bottom: ${({ theme }) => theme.spacing.medium};
`;

export const Link = styled(Classic)`
	border: none;
	cursor: pointer;
`;

export const Shows = styled.div`
	background-color: ${({ theme }) => theme.color.greyLight};
	position: relative;

	li {
		padding-left: ${({ theme }) => theme.spacing.medium};

		a {
			border-bottom: 1px solid ${({ theme }) => theme.color.grey};
			padding: ${({ theme }) => theme.spacing.medium};
			padding-left: 0;
			text-decoration: none;
			color: ${({ theme }) => theme.color.black};
			display: flex;
			align-items: center;
			justify-content: space-between;
			cursor: pointer;

			// @todo guessing font sizes by eye. Global sizing is a
			// bit off and I can't get exact measurements from figma
			.date {
				font-size: 18px;
				display: block;
				margin-bottom: 12px;
			}

			.location {
				font-size: 12px;
				display: block;
			}

			.tickets {
				font-size: 12px;
			}

			> svg {
				transform: rotate(270deg);
			}

			&:hover {
				opacity: 0.8;
			}
		}
	}

	footer {
		// @todo this looks fixed in the designs
		padding: ${({ theme }) => theme.spacing.medium};
		display: flex;
		align-items: center;
		justify-content: center;
	}
`;
