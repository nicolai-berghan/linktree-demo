import React from "react";
import * as s from "./_styled.footer";
import { ReactComponent as Logo } from "../../assets/logo.svg";

const Footer = () => {
	return (
		<s.Footer>
			<Logo />
		</s.Footer>
	);
};

export default Footer;
