import styled from "styled-components";

export const Footer = styled.footer`
	padding: ${({ theme }) => theme.spacing.large};
	display: flex;
	justify-content: center;
`;
