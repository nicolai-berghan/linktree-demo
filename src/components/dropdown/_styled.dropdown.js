import styled from "styled-components";

// @todo animate open/close of dropdown and possibly generate
// height dynamically, although it looks fixed in the designs.
export const DropDown = styled.div`
	height: ${({ isOpen }) => (isOpen ? "327px" : 0)};
	overflow: ${({ isOpen }) => (isOpen ? "auto" : "hidden")};
`;
