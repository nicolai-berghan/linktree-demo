import React from "react";
import * as s from "./_styled.dropdown";

const DropDown = ({ children, isOpen }) => {
	return <s.DropDown isOpen={isOpen}>{children}</s.DropDown>;
};

export default DropDown;
