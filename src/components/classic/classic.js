import React from "react";
import * as s from "./_styled.classic";

const Classic = ({ link }) => {
	return (
		<s.Classic href={link.url} target="_blank" className="u-mb-medium">
			<span>{link.title}</span>
		</s.Classic>
	);
};

// @todo include static type checking eg Flow or PropTypes
export default Classic;
