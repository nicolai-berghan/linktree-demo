import styled from "styled-components";

// There are lots of ways to resue styles here.
// I'm not sure the best way.
// I don't want these components to be coupled to each other.
// I could extend styles, create a global style, or just rewrite styles.
// My personal preference is to just rewrite the styles and keep each of
// the links separate from each other. Of course this might depend on how many
// types of links we're expecting. For 3 I'd rather keep them separate as they're
// likely to have different mark up. I'll extend for now, but that could cause issues
// if we're ever to make any updates to classic that we don't want to flow through.

// thought about changing <s.Classic/> to <s.Link/> and then wrapping as Link with Classic
// because I want to remove the margin as its causing issues on the other designs.
// I'm going to replace the margin with a utilty class instead.

export const Classic = styled.a`
	padding: ${({ theme }) => theme.spacing.small};
	width: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	background-color: ${({ theme }) => theme.color.green};
	border-radius: 4px;
	height: 48px;
	transition: all 0.1s;
	transition: all 0.1s;
	text-decoration: none;
	color: ${({ theme }) => theme.color.black};

	&:hover {
		background-color: ${({ theme }) => theme.color.black};
		color: ${({ theme }) => theme.color.green};
	}
`;
