import React from "react";
import * as s from "./_styled.profile";
import { user } from "../../helpers/mock-data";

const Profile = () => {
    return (
        <s.Profile>
			<div>
				<img
					src={user.profileImage.src}
					alt={user.profileImage.alt}
					title={user.profileImage.title}
				/>
			</div>
			<span>{user.username}</span>
		</s.Profile>
    );
};

export default Profile;