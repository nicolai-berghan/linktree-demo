import styled from "styled-components";

export const Profile = styled.header`
	display: flex;
	flex-direction: column;
	justify-content: center;
	padding: ${({ theme }) => theme.spacing.large};
	text-align: center;

	img {
		margin-bottom: ${({ theme }) => theme.spacing.small};
	}

	// @todo consider how to manage colors with users abilty to customize
	// maybe css variables, or import user themes.
	> span {
		color: ${({ theme }) => theme.color.black};
	}
`;
