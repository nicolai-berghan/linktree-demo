import React, { useState } from "react";
import * as s from "./_styled.links";
import { links } from "../../helpers/mock-data";
import Classic from "../classic/classic";
import MusicPlayer from "../music-player/music-player";
import ShowsList from "../shows-list/shows-list";

const Links = () => {
	// @todo I last minute saw that the musicPlayer and showsList had to close
	// each other when clicked so I have moved state here.
	// theres probably be a cleaner way to handle all this.
	const [musicPlayerIsOpen, setMusicPlayerIsOpen] = useState(false);
	const [showsListIsOpen, setShowsListIsOpen] = useState(false);

	function toggleMusicPlayer() {
		setMusicPlayerIsOpen(!musicPlayerIsOpen);
		setShowsListIsOpen(false);
	}

	function toggleShowsList() {
		setShowsListIsOpen(!showsListIsOpen);
		setMusicPlayerIsOpen(false);
	}

	return (
		<s.Links>
			{links.map((link, i) => {
				switch (link.type) {
					case "musicPlayer":
						return (
							<MusicPlayer
								key={i}
								link={link}
								musicPlayerIsOpen={musicPlayerIsOpen}
								toggleMusicPlayer={() => toggleMusicPlayer()}
							/>
						);
					case "showsList":
						return (
							<ShowsList
								key={i}
								link={link}
								showsListIsOpen={showsListIsOpen}
								toggleShowsList={() => toggleShowsList()}
							/>
						);
					default:
						return <Classic key={i} link={link} />;
				}
			})}
		</s.Links>
	);
};

export default Links;
