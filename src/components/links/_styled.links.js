import styled from "styled-components";

export const Links = styled.section`
	padding: ${({ theme }) => theme.spacing.medium};
`;
